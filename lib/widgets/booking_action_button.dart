import 'package:agenda_app/utils/utils.dart';
import 'package:flutter/material.dart';

class BookingActionButton extends StatelessWidget {
  const BookingActionButton({
    Key key,
    @required this.label,
    @required this.onPressed,
  }) : super(key: key);

  final String label;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: ElevatedButton(
          onPressed: onPressed,
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.symmetric(
              horizontal: 70.0,
              vertical: 6.0,
            ),
            primary: Utils.secondaryColor,
            shape: StadiumBorder(),
          ),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      ),
    );
  }
}
