import 'package:agenda_app/providers/services_provider.dart';
import 'package:agenda_app/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Calendar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ServicesProvider servicesProvider = Provider.of<ServicesProvider>(context);

    final int currentYear = servicesProvider.year;
    final String currentMonth =
        servicesProvider.months[servicesProvider.month - 1];
    List<Widget> calendarDays = [];

    for (var i = 1; i <= servicesProvider.countMonthDays; i++) {
      final current = DateTime(
        servicesProvider.year,
        servicesProvider.month,
        i,
        servicesProvider.currentDate.hour,
        0,
        0,
      );

      calendarDays.add(
        _CalendarDay(
          weekDay: servicesProvider.weekdays[current.weekday - 1],
          day: i,
          isSelected: servicesProvider.currentDate.compareTo(current) == 0,
          onTap: () => servicesProvider.day = i,
        ),
      );
    }

    return Container(
      child: Column(
        children: [
          SizedBox(height: 15.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: Icon(Icons.arrow_left_outlined, size: 30.0),
                color: Utils.secondaryColor,
                onPressed: () {
                  servicesProvider.changeMonth(false);
                },
              ),
              SizedBox(width: 15.0),
              Text(
                "$currentMonth ($currentYear)",
                style: TextStyle(fontSize: 22.0),
              ),
              SizedBox(width: 15.0),
              IconButton(
                icon: Icon(Icons.arrow_right_outlined, size: 30.0),
                color: Utils.secondaryColor,
                onPressed: () {
                  servicesProvider.changeMonth(true);
                },
              ),
            ],
          ),
          Container(
            height: 90.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: calendarDays,
            ),
          ),
        ],
      ),
    );
  }
}

class _CalendarDay extends StatelessWidget {
  const _CalendarDay({
    Key key,
    @required this.weekDay,
    @required this.day,
    @required this.isSelected,
    @required this.onTap,
  }) : super(key: key);

  final String weekDay;
  final int day;
  final bool isSelected;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 10.0,
        ),
        child: Column(
          children: [
            Text("$weekDay"),
            SizedBox(height: 5.0),
            Text(
              "$day",
              style: TextStyle(fontSize: 24.0),
            ),
            SizedBox(height: 10.0),
            isSelected
                ? Container(
                    height: 11.0,
                    width: 50.0,
                    decoration: BoxDecoration(
                      color: Utils.secondaryColor,
                    ),
                  )
                : Container(
                    height: 6.0,
                    width: 40.0,
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      border: Border(
                        bottom: BorderSide(
                          color: Utils.grayColor,
                          width: 1.0,
                        ),
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
